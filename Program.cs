﻿using System;

namespace ej_1_repetiva
{
    class Program
    {
        static void Main(string[] args)
        {
            // 1 crear un programa que escriba en pantalla los numeros del 1 al 10, "while".

            int n = 0;

            while (n < 10)
            {
                n++;
                Console.WriteLine(n);
            }


            // 2 crear un programa que calcule cuantas cifras tiene un numero

            int contador = 1;
            int z = 10;
            Console.WriteLine("ingrese un numero para saber la cantidad de cifras que tiene ");
            int num = int.Parse(Console.ReadLine());

            while (z <= num)
            {
                contador = contador + 1;
                z = z * 10;

            }
            Console.WriteLine($"Tiene:  {contador} cifras o digitos");




            // 3 crear un programa que de al usuario tres oportunidades para adivinar un numero del 1 al 23

            Random numero = new Random();

            int aleatorio = numero.Next(1, 23);
            int Numero_Ingresado = 24; // iniecie el numero que va ingresar el usuario para que pueda entar al bucle 

            int intentos = 0;

            Console.WriteLine("ingrese un numero del  1 al 23, tienes tres intentos para avidinar el numero");

            while (aleatorio != Numero_Ingresado && intentos < 3)
            {

                Numero_Ingresado = int.Parse(Console.ReadLine());

                if (Numero_Ingresado > aleatorio) Console.WriteLine(" el numero es mas bajo");
                if (Numero_Ingresado < aleatorio) Console.WriteLine(" el numero es mayor");

                intentos++;

            }

            if (Numero_Ingresado == aleatorio) Console.WriteLine($"correcto lo has logrado en {intentos} intentos");
            else Console.WriteLine($" FALLAST! has agotado tus 3 intentos  ");



            // 4 crear un programa que escriba en pantalla los numeros del 1 al 10 usando "Do.. whaile"

            int a = 0;
            do
            {
                a++;
                Console.WriteLine(a);
            } while (a < 10);





            // 5 crear un programa que pida numero positivos al usuario, y valla calculando la suma de todos ellos
            // (terminara cuando se teclea un numero negativo o cero).


            int suma = 0;
            Console.WriteLine("ingrese un numero: ");
            int numero = Convert.ToInt32(Console.ReadLine());

            while (numero != 0)
            {
                numero = Convert.ToInt32(Console.ReadLine());
                suma = suma + numero;

            }

            /*
            do
            {

                numero = Convert.ToInt32(Console.ReadLine());

                suma = suma + numero;

            } while (numero != 0);
            */

            Console.WriteLine(" la suma de los numero es: " + suma);





            // 6 crear un programa que pida al usuario su cedula y su contraseña (ambos numero), y no le permita seguir 
            // hasta que introduzca como cedula "00113764530" y como contraseña "1823"


            var cedula = "00113764530";
            var contr = "1823";

            Console.WriteLine(" ingrese su cedula para verificar usuario");
            string ced = (Console.ReadLine());

            Console.WriteLine(" ingrese la contraseña");
            string con = (Console.ReadLine());

            while (ced != cedula && con != contr)
            {
                Console.WriteLine($" usuario incorrecto \n");


                Console.WriteLine(" ingrese su cedula para verificar nuevamente ");
                ced = (Console.ReadLine());

                Console.WriteLine(" ingrese su cedula para verificar");
                con = (Console.ReadLine());

            }

            Console.WriteLine($" cedula y contraseña  correcta puede serguir adelante");

            Console.Read();




            // 7. crear un programa que muestre los primeros ocho numeros pares (tips: en cada pasada habra que aumentar 
            // de 2 en 2, o bien mostrar el doble de valores que hace de contador).

            int contador = 0;
            for (int i = 0; i < 16; i += 2)
            {
                contador++;
                Console.WriteLine($" el {contador} numeros pares es {i + 2} ");
            }





            // 8. crear un programa que las letras de la Z(mayuscula) a la A(mayuscula, descendiendo).

            char letras;
            for (letras = 'Z'; letras >= 'A'; letras--)
            {
                Console.WriteLine(letras);
            }





            //  9. crear un programa que escriba en pantalla los numeros del 1 al 50 que sean multiplo de 3.

            for (int i = 1; i <= 50; i++)
            {

                if (i % 3 == 0)
                {
                    Console.WriteLine($"Es multiplo 3 el: {i} ");
                }
            }



            // 10. crear un progrma que pregunte al usuario su edad y su año de nacimiento. si la edad que introduce
            // no es un numero valido, mostrara un mensaje de aviso, pero aun asi le preguntara su año de nacimiento.

            Console.WriteLine(" ingrese su edad: ");
            int edad = Convert.ToInt32(Console.ReadLine());

            if (edad > 200)
            {
                Console.WriteLine("como le ha hecho para manterse ");

                Console.WriteLine(" ingrese su año de nacimiento");
                int fe = Convert.ToInt32(Console.ReadLine());
                int res = 2020 - fe;
                if (res != edad) Console.WriteLine("su edad actual es: " + res);

            }
            else
            {
                Console.WriteLine(" ingrese su año de nacimiento");
                int fecha = Convert.ToInt32(Console.ReadLine());

            }




            // 11. crear un programa que pida al usuario su nombre complecto y año de nacimiento y muestre un mensaje
            // ( su nombre es: nombre y su edad es: edad

            Console.WriteLine(" ingrese su nombre: ");
            string NOMBRE = Console.ReadLine();

            Console.WriteLine(" ingrese su edad: ");
            int EDAD = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine($" Su nombre es: {NOMBRE} y su edad es: {EDAD} ");



        }
    }
}
